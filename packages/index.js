/**
 * @author hjm100
 * Date: 19/03/27
 */
//全局引入css样式
import "./style/common/var.css";
import "./style/reset.css";
import hjmButton from "./button/index.js";

const components = [hjmButton];

const install = Vue => {
  components.forEach(Component => {
    Vue.use(Component);
  });
};

if (typeof window !== "undefined" && window.Vue) {
  install(window.Vue);
}
// 也可以单独引入组件使用
export default {
  install,
  hjmButton
};
