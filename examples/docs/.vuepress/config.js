module.exports = {
    title: 'HJM VUE UI',
    description: '我是hjm开发的UI组件',
    // dest:'../',
    base:'/hjm-vue-ui/',
    markdown:{
        lineNumbers:true,
    },
    themeConfig: {
        nav: [
          { text: 'Home', link: '/' },
          { text: 'GitHub', link: 'https://gitee.com/hjm100/hjm-vue-ui' },
          { 
            text: 'About', 
            items: [
                { text: '开发者网站', link: 'https://hjm100.cn'},
                { text: '开发者简介', link: 'https://hjm100.cn/about'}
            ]
          }
        ],
        sidebar: [
            {
                title: '开发指南',
                children: ['/','/giud','/install','/start','/logs']
            },{
                title: '表单组件',
                children: ['/button']
            },{
                title: 'VuePress',
                children: ['/VuePress']
            }
        ]
    }
}